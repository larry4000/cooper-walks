let app = new Vue({
    el: '#app',
    data: {
        walks: [],
        weekDays: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
        weekend: ["Saturday", "Sunday"],
        selectedDay: 0,
        selectedWeek: 0,
        weeks: ["W1", "W2", "W3", "W4", "W5"]
    },
    created: function() {
        //console.log('debug')
        // Default day to today
        if (this.weekend.includes(dayjs().format('dddd'))) {
            this.selectedDay = 0
        } else {
            this.selectedDay = dayjs().format('d') - 1
        }
        //Restore state from local storage
        if (localStorage.getItem('walks')) {
            this.walks = JSON.parse(localStorage.getItem('walks'))
        } else {
            // Initialise zero values for every day this week
            for (let i = 0; i < this.weeks.length; i++) {
                Vue.set(this.walks, i, new Array(5).fill(0))
            }
        }

        if (localStorage.getItem('selectedWeek')) {
            this.selectedWeek = parseInt(localStorage.getItem('selectedWeek'))
        }
    },
    computed: {
        today: function () {
            if (this.weekend.includes(dayjs().format('dddd'))) {
                return "Friday"
            } else {
                return dayjs().format('dddd')
            }
        },
        message: function () {
            return 'Cooper is having ' + this.walks[this.selectedWeek][this.selectedDay] + ' walkies on ' + this.weekDays[this.selectedDay] 
        },
        totalCost: function() {
            let total = 0;
            for (let i =0; i < this.weeks.length; i++) {
                for (let j = 0; j < this.weekDays.length; j++) {
                    total += this.walks[i][j]
                }
            }
            return total * 10;
        }
    },
    methods: {
        addWalk: function () {
            this.walks[this.selectedWeek].splice( 
                this.selectedDay,
                1,
                this.walks[this.selectedWeek][this.selectedDay] + 1
            )
            localStorage.setItem('walks', JSON.stringify(this.walks))
        },
        removeWalk: function () {
            if (this.walks[this.selectedWeek][this.selectedDay] == 0) {
                return
            }
            this.walks[this.selectedWeek].splice(
                this.selectedDay,
                1,
                this.walks[this.selectedWeek][this.selectedDay] - 1
            )
            localStorage.setItem('walks', JSON.stringify(this.walks))
        },
        setDay: function(day) {
            this.selectedDay = this.weekDays.indexOf(day)
        },
        setWeek: function(week) {
            this.selectedWeek = this.weeks.indexOf(week)
            localStorage.setItem('selectedWeek', String(this.selectedWeek))
        },
        getWeeklyTotal: function(week) {
            let total = 0
            const weekIndex = this.weeks.indexOf(week)
            for (let i = 0; i < this.walks[weekIndex].length; i++) {
                total += this.walks[weekIndex][i]
            }
            return total * 10
        },
        isActiveWeek: function (week) {
            return week === this.weeks[this.selectedWeek]
        },
        isActiveDay: function (day) {
            return day === this.weekDays[this.selectedDay]
        }
    }
})