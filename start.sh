#!/bin/bash
docker stop cooper-walks || true && docker rm cooper-walks || true
docker run -d --name cooper-walks --restart unless-stopped -l "traefik.http.routers.cooper-walks.rule=Host(\`cooper-walks.com\`)" -l "traefik.http.routers.cooper-walks.tls.certresolver=myresolver" -p 80 cooper-walks
